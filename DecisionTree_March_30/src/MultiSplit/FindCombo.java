package MultiSplit;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class FindCombo {
	
	
	public static void main(String[] args){
		Double[] d = {1.0,2.0,3.0,4.0,5.0};
		int num = 3;
		List<Double> source = Arrays.asList(d);
		System.out.println(findCombinationFromSortedList(source,num));
		
	}

	public static List<List<Double>> findCombinationFromSortedList(List<Double> source, int num ){
		/*
		 * return all the combiantion of list, in accending order
		 * list has been sorted
		 * num is the num of combination
		 * all are different values
		 */
		
		//check boundries
		if(source == null || num == 0 || num > source.size()){
			return null;
		}
		
		//init
		List<List<Double>> result = new LinkedList<List<Double>>();
		List<Double> list= new LinkedList<Double>();
		
		//use helper
		helper(result,list, 0, num,source);
		//System.out.println("combo num:"+ result.size());
		return result;
	}
	
	public static void helper(List<List<Double>> result, List<Double> list, int pos, int num, List<Double> source){
		if(list.size() == num){
			result.add(new LinkedList<Double>(list));
			return;
		}
		for(int i = pos; i < source.size(); i++){
			double d = source.get(i);
			list.add(d);
			helper(result,list,i+1,num,source);
			list.remove(list.size() - 1);
		}
	}
	
}
