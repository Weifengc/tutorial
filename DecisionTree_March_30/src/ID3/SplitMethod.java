package ID3;

import java.util.List;

import team4.bean.Rules;
import team4.bean.TrainData;

public interface SplitMethod {
	public List<Rules>returnRules(String trainDataFile, String attributesFile);
	public List<Rules> returnRules(List<TrainData> dataList, String attributesFile);
}
