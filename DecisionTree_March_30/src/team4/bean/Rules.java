package team4.bean;

import team4.interfaces.FileReadable;


public class Rules extends Data{

	int pos;
	double start;
	double end;
	String cat;
	
	public Rules(){
		
	}
	public Rules(String[] ss){
		this.attributes = ss;
		pos = Integer.parseInt(ss[0]);
		start = Double.parseDouble(ss[1]);
		end = Double.parseDouble(ss[2]);
		cat = ss[3];
	}
	public Rules(String input){
		this(input.split(","));
	}
	@Override
	public String writeToFile(){
		StringBuilder sb = new StringBuilder();
		sb.append(this.pos);
		sb.append(",");
		sb.append(this.start);
		sb.append(",");
		sb.append(this.end);
		sb.append(",");
		sb.append(this.cat);
		return sb.toString();
	}
	
	@Override
	public FileReadable updateByString(String input) {
		return new Rules(input.split(","));
	}
	@Override
	protected Data copy() {
		Rules r = new Rules(this.attributes);
		return r;
	}

}
