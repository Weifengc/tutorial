package team4.bean;

import java.util.Comparator;

public class CompareByPos implements Comparator<TrainData> {
	int pos;

	public CompareByPos(Attribute a) {
		this(a.getPos());
	}
	public CompareByPos(int pos) {
		this.pos = pos;
	}

	@Override
	public int compare(TrainData o1, TrainData o2) {
		double d1 = Double.parseDouble(o1.getAttributes()[pos]);
		double d2 = Double.parseDouble(o2.getAttributes()[pos]);
		if (d1 > d2) {
			return 1;
		} else if (d1 < d2) {
			return -1;
		} else {
			return 0;
		}
	}

}
