package team4.bean;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import team4.interfaces.FileReadable;


public class TrainData extends Data{
	
	
	public TrainData(String[] attributes, String label){
		this.attributes = new String[attributes.length];
		System.arraycopy(attributes,0,this.attributes,0,attributes.length);
		this.label = label;
	}
	
	public TrainData(){
	}
	
	@Override
	public FileReadable updateByString(String input) {
		//put the last to last to be the label
		String la;
		String[] ss = input.split(",");
		String[] att = new String[ss.length -1];
		System.arraycopy(ss,0, att,0,ss.length -1);
		la = ss[ss.length - 1];
		return new TrainData(att,la);
	}

	@Override
	protected Data copy() {
		return new TrainData(this.attributes, this.label);
	}

	public static boolean isLabelSame(List<TrainData> list){
		if(list == null || list.size() == 0){
			System.out.println("list are null");
			return false;
		}
		String label = list.get(0).label;
		for(TrainData d : list){
			if(!label.equals(d.getLabel())){
				return false;
			}
		}
		return true;
	}
	
	public static String mostLabel(List<TrainData> list){
		Map<String,Integer> map = new HashMap<String, Integer>();
		for(TrainData d : list){
			String label = d.getLabel();
			if(map.containsKey(label)){
				map.put(label,map.get(label)+1);
			}else{
				map.put(label,1);
			}
		}
		
		int max = 0;
		String label = "";
		for(String ls : map.keySet()){
			if(map.get(ls) > max){
				label = ls;
				max = map.get(ls);
			}
		}
		return label;
	}

	public static List<TrainData> getDataWithAttribute(List<TrainData> examples,
			String val,int pos) {
		List<TrainData> list = new LinkedList<TrainData>();
		for(TrainData d : examples){
			if(d.getAttributes() != null && d.getAttributes()[pos].toLowerCase().equals(val.toLowerCase())){
				list.add(d);
			}
		}
		return list;
	}

}
