package team4.interfaces;

import java.util.List;

import team4.bean.Rules;

public interface CategoryAble {
	public CategoryAble changeToCategory(String fileName);
	
	public CategoryAble changeToCategory(List<Rules> list);
}
